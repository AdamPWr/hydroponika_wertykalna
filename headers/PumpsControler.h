#pragma once
#include<sys/types.h>
#include<vector>
#include"driver/ledc.h"
#include<math.h>

class PumpsControler
{

    const std::vector<gpio_num_t> PUMPS_OUTPUTS_PINS;
    ledc_timer_config_t* timer_config;
    ledc_channel_config_t* channels_config;

    const ledc_timer_bit_t TIMER_DUTY_RESOLUTION = LEDC_TIMER_12_BIT;
    const int MAX_DUTY_RESOLUTION = std::pow(2,int(TIMER_DUTY_RESOLUTION)) - 1;

    u_int8_t pinToChannel(gpio_num_t pin);

    public:
    PumpsControler(const std::vector<gpio_num_t>& pumps_output_pins);

    void turnOnPumpWithIntensity(const gpio_num_t pump_pin, u_int8_t intensity_in_procentage);
    void turnOffPump(const gpio_num_t pump_pin);


    ~PumpsControler();


};