#include"../headers/TDSDriver.h"
#include<math.h>

TDSDriver::TDSDriver() : resolution_in_bits(ADC_WIDTH_BIT_12), channel_number(ADC1_CHANNEL_7)
{
    adcSetup(resolution_in_bits, channel_number);
}

TDSDriver::TDSDriver(adc_bits_width_t p_resolution_in_bits, adc1_channel_t channel_number) : 
resolution_in_bits(p_resolution_in_bits),
channel_number(channel_number)
{
    adcSetup(resolution_in_bits, channel_number);
}

void TDSDriver::adcSetup(adc_bits_width_t adc_bits_width, adc1_channel_t channel_number)
{
    adc1_config_width((adc_bits_width_t)std::pow(2.0,adc_bits_width));
    adc1_config_channel_atten(channel_number, ADC_ATTEN_DB_0);
}

u_int16_t TDSDriver::readRawAnalogMeasurment()
{
    return adc1_get_raw(channel_number);
}

double TDSDriver::readVoltage()
{
    return adc1_get_raw(channel_number) * refVoltage / std::pow(2.0,resolution_in_bits);
}

float TDSDriver::calculateTDSVaule(float water_temperature)
{
    double tdsVal = 0;

    float rawEc = readVoltage();
    float temperatureCoefficient = 1.0 + 0.02 * (water_temperature - 25.0);
    float ec = (rawEc / temperatureCoefficient) * ecCalibration;
    tdsVal = (133.42 * pow(ec, 3) - 255.86 * ec * ec + 857.39 * ec) * 0.5;

    return tdsVal;
}
TDSDriver::~TDSDriver()
{
}