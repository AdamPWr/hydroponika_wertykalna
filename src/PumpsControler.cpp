#include"../headers/PumpsControler.h"
#include "esp_err.h"
#include<algorithm>
#include "esp_log.h"


PumpsControler::PumpsControler(const std::vector<gpio_num_t>& pumps_output_pins): PUMPS_OUTPUTS_PINS(pumps_output_pins)
{

    const auto number_of_channels = PUMPS_OUTPUTS_PINS.size();

    timer_config = new ledc_timer_config_t;
    channels_config = new ledc_channel_config_t[number_of_channels];

    timer_config->speed_mode = LEDC_HIGH_SPEED_MODE;
    timer_config->duty_resolution = TIMER_DUTY_RESOLUTION;
    timer_config->timer_num = LEDC_TIMER_0;
    timer_config->freq_hz = 19531;
    timer_config->clk_cfg = LEDC_USE_APB_CLK;
    ESP_ERROR_CHECK(ledc_timer_config(timer_config));

    for(int i = 0; i< number_of_channels; ++i)
    {
        channels_config[i].gpio_num = PUMPS_OUTPUTS_PINS[i];
        channels_config[i].channel = ledc_channel_t(i);
        channels_config[i].duty = 0;
        channels_config[i].speed_mode = LEDC_HIGH_SPEED_MODE;
        channels_config[i].intr_type = LEDC_INTR_DISABLE;
        channels_config[i].timer_sel = LEDC_TIMER_0;
        ESP_ERROR_CHECK(ledc_channel_config(&channels_config[i]));
    }
}

u_int8_t PumpsControler::pinToChannel(gpio_num_t pin)
{
    auto channel = std::find(PUMPS_OUTPUTS_PINS.begin(), PUMPS_OUTPUTS_PINS.end(), pin);

    if(channel != PUMPS_OUTPUTS_PINS.end())
    {
        int index =  channel - PUMPS_OUTPUTS_PINS.begin();
        return index;
    }
    else
    {
        ESP_LOGI("PUMPS_CONTROLER", "invalid concersion form pump pin to channel");
        abort();
    }
}

void PumpsControler::turnOnPumpWithIntensity(const gpio_num_t pump_pin, u_int8_t intensity_in_procentage = 0)
{
    if(intensity_in_procentage < 0 or intensity_in_procentage > 100)
        return;
    ledc_channel_t pump_channel = ledc_channel_t(pinToChannel(pump_pin));
    int duty = MAX_DUTY_RESOLUTION * 0.75 + MAX_DUTY_RESOLUTION *0.25 *intensity_in_procentage/100;
    printf("duty = %d channel = %d\n", duty, int(pump_channel));
    ledc_set_duty(LEDC_HIGH_SPEED_MODE, pump_channel, duty);
    ledc_update_duty(LEDC_HIGH_SPEED_MODE, pump_channel);
}

void PumpsControler::turnOffPump(const gpio_num_t pump_pin)
{   
    constexpr auto DUTY = 0;
    ledc_channel_t pump_channel = ledc_channel_t(pinToChannel(pump_pin));
    ledc_set_duty(LEDC_HIGH_SPEED_MODE, pump_channel, DUTY);
    ledc_update_duty(LEDC_HIGH_SPEED_MODE, pump_channel);
}

PumpsControler::~PumpsControler()
{
    delete timer_config;
    delete[] channels_config;
}