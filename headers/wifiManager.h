#pragma once
#include"../headers/stateManager.h"


namespace WIFIManager
{
    void startWIFIManager();
    void connectSTA(void *pvParameter);
    void disConnectSTA(void *pvParameter);
    void connectAP(void *pvParameter);
    void disConnectAP(void *pvParameter);
}
