#pragma once


#include "mqtt_client.h"
#include<string>

class MQTTClientHydroponika
{
    esp_mqtt_client_config_t mqtt_cfg{};
    const char* URI = "mqtt://lunaproject.one";
    const int PORT  = 1883;
    static esp_mqtt_client_handle_t client;
    uint8_t chipid[6];
    const char* login = "ubuntu02project";
    const char* password = "YasuoMain";
    static std::string subscribe_topic;
    static std::string will_topic;
    static std::string publish_topic;
    static std::string chipIdString;
    static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);

    public:
    static const char *TAG;
    static std::string data_topic;


    MQTTClientHydroponika();
    void setup();
    static int publish(const std::string topic, const std::string data, int qos = 2, int retain = 0);
    static int subscribe(const std::string topic, int qos = 2);
    static int unSubscribe(const std::string topic, int qos = 2);

};