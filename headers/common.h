#pragma once
#include"../headers/systemComponents.h"
#include<memory>
namespace common
{
    
    void makePins12_14UsebleAsGpio();
    void makeSensorsMeasurmentsAndPublishData(std::shared_ptr<SystemComponents> systemComponents);
    void waterPumpTask(void * pump_controler);


}