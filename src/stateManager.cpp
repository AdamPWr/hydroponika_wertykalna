#include"../headers/stateManager.h"

void StateManager::setConnectionState(const ConnectionState state)
{
    connectionState = state;
}
ConnectionState StateManager::getConnectionState()
{
    return connectionState;
}

void StateManager::setTempSensorState(const TempSensorState state)
{
    tempSensorState  = state;
}

TempSensorState StateManager::getTempSensorState()
{
    return tempSensorState;
}