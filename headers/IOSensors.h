#pragma once
#include"hal/gpio_types.h"
#include "driver/adc.h"
#include<vector>

namespace IO_SENSORS
{


    static gpio_num_t DS18B20_PIN = GPIO_NUM_25;
    static gpio_num_t WATER_LEVEL_PIN_MIN = GPIO_NUM_12; //SL7 // zajete przez tds
    static gpio_num_t WATER_LEVEL_PIN_MAX = GPIO_NUM_35;
    static adc1_channel_t TDS_CHANNEL = ADC1_CHANNEL_6; // coresponding gpio34
    static std::vector<gpio_num_t> PUMPS_OUTPUTS = 
    {
        GPIO_NUM_13,// water pump
        GPIO_NUM_14, //zajete juz
        GPIO_NUM_15,
        GPIO_NUM_17,
        GPIO_NUM_27
    };

}
