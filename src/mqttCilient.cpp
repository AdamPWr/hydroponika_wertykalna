#include"../headers/mqttClient.h"
#include "esp_log.h"
#include<esp_system.h>
#include<map>
#include"../headers/JSONParser.h"

const char* MQTTClientHydroponika::TAG = "MQTT_EXAMPLE";
esp_mqtt_client_handle_t MQTTClientHydroponika::client = {};
std::string MQTTClientHydroponika::will_topic = {};
std::string MQTTClientHydroponika::publish_topic = {};
std::string MQTTClientHydroponika::subscribe_topic = {};
std::string MQTTClientHydroponika::data_topic = {};
std::string MQTTClientHydroponika::chipIdString = {};

static void log_error_if_nonzero(const char *message, int error_code)
{
    if (error_code != 0) {
        ESP_LOGE(MQTTClientHydroponika::TAG, "Last error %s: 0x%x", message, error_code);
    }
}

void MQTTClientHydroponika::mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    esp_mqtt_event_handle_t event = static_cast<esp_mqtt_event_handle_t>(event_data);
    //esp_mqtt_client_handle_t client = event->client;
    int msg_id = 0; // nwm czy powininem to tu inicjalizowac
    switch ((esp_mqtt_event_id_t)event_id) {
    case MQTT_EVENT_CONNECTED:
    
        ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
        // msg_id = esp_mqtt_client_publish(client, "/topic/qos1", "data_3", 0, 1, 0);
        msg_id = publish(publish_topic,chipIdString);
        ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
        msg_id = subscribe(subscribe_topic);
        ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);


        // msg_id = esp_mqtt_client_subscribe(client, "/topic/qos0", 0);
        // ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

        // msg_id = esp_mqtt_client_subscribe(client, "/topic/qos1", 1);
        // ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

        // msg_id = esp_mqtt_client_unsubscribe(client, "/topic/qos1");
        // ESP_LOGI(TAG, "sent unsubscribe successful, msg_id=%d", msg_id);
        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
        // msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
        ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI(TAG, "MQTT_EVENT_DATA");
        printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
        printf("DATA=%.*s\r\n", event->data_len, event->data);
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
        if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
            log_error_if_nonzero("reported from esp-tls", event->error_handle->esp_tls_last_esp_err);
            log_error_if_nonzero("reported from tls stack", event->error_handle->esp_tls_stack_err);
            log_error_if_nonzero("captured as transport's socket errno",  event->error_handle->esp_transport_sock_errno);
            ESP_LOGI(TAG, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));

        }
        break;
    default:
        ESP_LOGI(TAG, "Other event id:%d", event->event_id);
        printf("EVENT MESSAGE=%.*s\r\n", event->data_len, event->data);
        break;
    }
}

MQTTClientHydroponika::MQTTClientHydroponika()
{
    esp_efuse_mac_get_default(chipid);
    printf("%02x:%02x:%02x:%02x:%02x:%02x\n",chipid[0], chipid[1], chipid[2], chipid[3], chipid[4], chipid[5]);
    for(auto& digit : chipid)
    {
        chipIdString += std::to_string(digit);
    }
    data_topic = "verticalHydroponic/data/" + chipIdString;
    publish_topic = "start/verticalHydroponic/" + chipIdString;
    subscribe_topic = "verticalHydroponic/" + chipIdString;
    will_topic = "dead/verticalHydroponic/" + chipIdString;
}

void MQTTClientHydroponika::setup()
{
    printf("dupa1.1");

    std::map<std::string, std::string> willMessageMap;
    willMessageMap.insert({"esp_id", chipIdString});
    std::string willMessageString = JSONParser::parseToJson(willMessageMap);
    printf("will \n %s", willMessageString.c_str());
    mqtt_cfg.uri = URI;
    mqtt_cfg.port = PORT;
    mqtt_cfg.username = login;
    mqtt_cfg.password = password;
    mqtt_cfg.lwt_topic = will_topic.c_str();
    mqtt_cfg.lwt_msg = willMessageString.c_str();
    mqtt_cfg.lwt_msg_len = willMessageString.length();
    mqtt_cfg.transport = MQTT_TRANSPORT_OVER_TCP;
    //mqtt_cfg.keepalive 

    client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(client, MQTT_EVENT_ANY, mqtt_event_handler, MQTTClientHydroponika::client);
    esp_mqtt_client_start(client);
    
}

int MQTTClientHydroponika::publish(const std::string topic, const std::string data, int qos, int retain)
{
    printf("wysylam r\n %s", data.c_str());
    int ret = esp_mqtt_client_publish(client, topic.c_str(), data.c_str(), data.length(), qos, retain);
    if(ret == -1)
        printf("Failed to publish message \n");
    printf("ret %d \n", ret);
    return ret;
}

int MQTTClientHydroponika::subscribe(const std::string topic, int qos)
{
    return esp_mqtt_client_subscribe(client, topic.c_str(), qos);
}

int MQTTClientHydroponika::unSubscribe(const std::string topic, int qos)
{
    return esp_mqtt_client_unsubscribe(client, topic.c_str());
}