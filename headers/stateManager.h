#pragma once

enum class ConnectionState
{
    IDLE,
    STA_CONNECTED,
    STA_DISCONNECTED,
    AP_CONNECTED,
    AP_DISCONNECTED
};

enum class TempSensorState
{
    IDLE,
    FOUND_ANY,
    NOT_FOUND_ANY
};


class StateManager
{
private:
    ConnectionState connectionState;
    TempSensorState tempSensorState;

public:
    void setConnectionState(const ConnectionState state);
    ConnectionState getConnectionState();
    void setTempSensorState(const TempSensorState state);
    TempSensorState getTempSensorState();
};