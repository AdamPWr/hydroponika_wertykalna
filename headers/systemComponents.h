#pragma once
#include"../headers/ds18b20Driver.h"
#include"../headers/mqttClient.h"
#include"../headers/IOSensors.h"
#include"../headers/TDSDriver.h"
#include"../headers/waterLevelSensorDriver.h"
#include"../headers/PumpsControler.h"
#include"../headers/wifiManager.h"
#include"../headers/stateManager.h"


struct SystemComponents
{
public:

    StateManager stateManager;
    MQTTClientHydroponika mqttclient;
    DS18b20Driver ds;
    TDSDriver tdsDriver;
    WaterLevelSensorDriver waterLevelDriver;
    PumpsControler pumpsControler;

    SystemComponents();
    void waitForEstablishConnectionInSTAModeAndInitMQTTClient();

private:
};