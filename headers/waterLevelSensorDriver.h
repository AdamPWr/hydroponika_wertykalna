# pragma once

#include<hal/gpio_types.h>

class WaterLevelSensorDriver
{
    gpio_num_t WATER_LEVEL_MIN_SENSOR_PIN;
    gpio_num_t WATER_LEVEL_MAX_SENSOR_PIN;

    void setup();

    public:
    WaterLevelSensorDriver(gpio_num_t sensor_pin_min, gpio_num_t sensor_pin_max);
    int checkWaterLevelMin();
    int checkWaterLevelMax();
};