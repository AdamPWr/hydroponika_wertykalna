#include"../headers/JSONParser.h"
#include"cJSON.h"


std::string JSONParser::parseFromJson(std::string raw_content)
{
//     cJSON *elem;
//     cJSON temp;
//     cJSON *tds;
//     cJSON *waterLevel;
//     cJSON *root = cJSON_Parse(raw_content.c_str());
//     int num_of_transitions = cJSON_GetArraySize(root);

//     std::vector<Color> transition;
//     Color tempColor;
//     transition.reserve(num_of_transitions);
//     for (u_int8_t i = 0; i < num_of_transitions; i++) 
//     {
//         elem = cJSON_GetArrayItem(root, i);
//         r = cJSON_GetObjectItem(elem, "r");
//         g = cJSON_GetObjectItem(elem, "g");
//         b = cJSON_GetObjectItem(elem, "b");
//         tempColor.r = r->valueint;
//         tempColor.g = g->valueint;
//         tempColor.b = b->valueint;
//         printf("%d\n", tempColor.r);
//         printf("%d\n", tempColor.g);
//         printf("%d\n", tempColor.b);
//         transition.push_back(tempColor);
//     }

//     return transition;
    return "";
}

std::string JSONParser::parseToJson(std::map<std::string, float>& vaules)
{
    std::string parsedContent;
    cJSON* json  = cJSON_CreateObject();
    for(auto& element : vaules)
    {
        // printf("dupa3 %s %s\n", element.first.c_str(), std::to_string(element.second).c_str());
        cJSON_AddStringToObject(json,element.first.c_str(), std::to_string(element.second).c_str());
    }
    parsedContent = cJSON_Print(json);
    printf(parsedContent.c_str());
    cJSON_Delete(json);
    return parsedContent;
}

std::string JSONParser::parseToJson(std::map<std::string, std::string>& vaules)
{
    std::string parsedContent;
    cJSON* json  = cJSON_CreateObject();
    for(auto& element : vaules)
    {
        // printf("dupa3 %s %s\n", element.first.c_str(), std::to_string(element.second).c_str());
        cJSON_AddStringToObject(json,element.first.c_str(), element.second.c_str());
    }
    parsedContent = cJSON_Print(json);
    printf(parsedContent.c_str());
    cJSON_Delete(json);
    return parsedContent;
}