#pragma once
#include"ds18b20.h"
#include<owb.h>
// #include "ds18b20.h"

class DS18b20Driver
{
    const static u_int8_t MAX_NUM_OF_DEVICES {1};
    const static DS18B20_RESOLUTION RESOLUTION {DS18B20_RESOLUTION_12_BIT};
    OneWireBus*  owb;
    owb_rmt_driver_info rmt_driver_info;
    gpio_num_t DS1820B_PIN;// = GPIO_NUM_36; // range 0..33
    int num_devices = 0;
    DS18B20_Info * devices[MAX_NUM_OF_DEVICES] = {0};

    void setup();
    void teardown();
    public:

    DS18b20Driver(gpio_num_t ds_pin);
    void searchDevices();
    float readTemp();
    ~DS18b20Driver();
};