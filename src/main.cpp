#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include"../headers/common.h"
#include<memory>

#include"../headers/systemComponents.h"
#include<cJSON.h>
#include"../headers/led_strip.h"

extern "C" {
    void app_main(void);
}

std::shared_ptr<SystemComponents> systemComponents;

void app_main()
{
    common::makePins12_14UsebleAsGpio();
    systemComponents = std::make_shared<SystemComponents>();
    systemComponents->waitForEstablishConnectionInSTAModeAndInitMQTTClient();

    TaskHandle_t waterPumpTaskhandle = NULL;
    xTaskCreate(common::waterPumpTask,"waterPumpTask", 2048,&systemComponents->pumpsControler,5, &waterPumpTaskhandle);

    while(true)
    {   
        common::makeSensorsMeasurmentsAndPublishData(systemComponents);
        vTaskDelay(1000.0 * 60/ portTICK_PERIOD_MS);
    }
}