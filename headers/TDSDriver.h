#pragma once
#include<sys/types.h>
#include "driver/adc.h"
#include"hal/adc_types.h"

class TDSDriver
{
private:
    //u_int8_t tds_inout_pin;
    adc_bits_width_t resolution_in_bits;
    const adc1_channel_t channel_number;
    const double refVoltage = 3.3;
    const double  tds_factor = 0.5;
    const float ecCalibration = 1.0;

    void adcSetup(adc_bits_width_t adc_bits_width_t, adc1_channel_t channel_number);
    u_int16_t readRawAnalogMeasurment();
    double readVoltage();
public:
    TDSDriver();
    TDSDriver(adc_bits_width_t adc_bits_width_t, adc1_channel_t channel_number);

    float calculateTDSVaule(float water_temperature);
    ~TDSDriver();
};

