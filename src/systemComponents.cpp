#include"../headers/systemComponents.h"

SystemComponents::SystemComponents():
ds(IO_SENSORS::DS18B20_PIN),
tdsDriver(ADC_WIDTH_BIT_12, IO_SENSORS::TDS_CHANNEL),
waterLevelDriver(IO_SENSORS::WATER_LEVEL_PIN_MIN, IO_SENSORS::WATER_LEVEL_PIN_MAX),
pumpsControler(IO_SENSORS::PUMPS_OUTPUTS)
{
    WIFIManager::startWIFIManager();
}

void SystemComponents::waitForEstablishConnectionInSTAModeAndInitMQTTClient()
{
       while(true)
        {
            if(stateManager.getConnectionState() == ConnectionState::STA_CONNECTED)
            {
                mqttclient.setup();
                break;
            }
            else{
                printf("Waiting for conncection in STA mode. Current state_id:%d\n", (int)stateManager.getConnectionState());
                vTaskDelay(5000.0 / portTICK_PERIOD_MS);
                continue;
            }
        }
}