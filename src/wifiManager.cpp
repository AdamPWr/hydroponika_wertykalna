#include"../headers/wifiManager.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include<wifi_manager.h>
#include"../headers/common.h"
#include<memory>

extern std::shared_ptr<SystemComponents> systemComponents;

namespace WIFIManager
{
    void startWIFIManager()
    {
        wifi_manager_start();
        printf("WIFI MANAGER Start");
        wifi_manager_set_callback(WM_EVENT_STA_GOT_IP, &connectSTA);
        wifi_manager_set_callback(WM_EVENT_STA_DISCONNECTED, &disConnectSTA);
        wifi_manager_set_callback(WM_ORDER_START_AP, &connectAP);
        wifi_manager_set_callback(WM_ORDER_STOP_AP, &disConnectAP);
    }

    void connectSTA(void *pvParameter)
    {
        ESP_LOGI("WIFI-Manager", "I have a connection!");
        if(systemComponents)
            systemComponents->stateManager.setConnectionState(ConnectionState::STA_CONNECTED);
        else{ printf("NULL DUPA ");}
    }

    void disConnectSTA(void *pvParameter)
    {
        ESP_LOGI("WIFI-Manager", "disConnectSTA!");
        systemComponents->stateManager.setConnectionState(ConnectionState::STA_DISCONNECTED);
    }

    void connectAP(void *pvParameter)
    {
        systemComponents->stateManager.setConnectionState(ConnectionState::AP_CONNECTED);
        ESP_LOGI("WIFI-Manager", "ConnectAP");
    }
    
    void disConnectAP(void *pvParameter)
    {
        systemComponents->stateManager.setConnectionState(ConnectionState::AP_DISCONNECTED);
        ESP_LOGI("WIFI-Manager", "disConnectAP");
    }
}
