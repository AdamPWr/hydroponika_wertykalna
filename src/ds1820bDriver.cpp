#include"../headers/ds18b20Driver.h"
#include "esp_log.h"

DS18b20Driver::DS18b20Driver(gpio_num_t ds_pin) : DS1820B_PIN(ds_pin)
{
    setup();
}

void DS18b20Driver::setup()
{

    owb = owb_rmt_initialize(&rmt_driver_info, DS1820B_PIN, RMT_CHANNEL_1, RMT_CHANNEL_0);
    owb_use_crc(owb, true);  // enable CRC check for ROM code
    searchDevices();
    for (int i = 0; i < num_devices; ++i)
    {
        DS18B20_Info * ds18b20_info = ds18b20_malloc();  // heap allocation
        devices[i] = ds18b20_info;

        if (num_devices == 1)
        {
            printf("Single device optimisations enabled\n");
            ds18b20_init_solo(ds18b20_info, owb);          // only one device on bus
        }
        else
        {
            // ds18b20_init(ds18b20_info, owb, device_rom_codes[i]); // associate with bus and device
        }
        ds18b20_use_crc(ds18b20_info, true);           // enable CRC check on all reads
        ds18b20_set_resolution(ds18b20_info, RESOLUTION);
    }

}

void DS18b20Driver::searchDevices()
{
      // Find all connected devices
    printf("Find devices:\n");
    OneWireBus_ROMCode device_rom_codes[MAX_NUM_OF_DEVICES] = {0};
    OneWireBus_SearchState search_state = {0};
    bool found = false;
    printf("dupa2\n");
    // auto status = owb_search_first(owb, &search_state, &found);
    printf("owb_search_first returned %d\n", owb_search_first(owb, &search_state, &found));
    //printf("status %d found: %s \n",status, found ? "true":"false");
    while (found)
    {
        char rom_code_s[17];
        owb_string_from_rom_code(search_state.rom_code, rom_code_s, sizeof(rom_code_s));
        printf("  %d : %s\n", num_devices, rom_code_s);
        device_rom_codes[num_devices] = search_state.rom_code;
        ++num_devices;
        owb_search_next(owb, &search_state, &found);
    }
    printf("Found %d device%s\n", num_devices, num_devices == 1 ? "" : "s");

    // if (num_devices == 1)
    // {
    //     // For a single device only:
    //     OneWireBus_ROMCode rom_code;
    //     owb_status status = owb_read_rom(owb, &rom_code);
    //     if (status == OWB_STATUS_OK)
    //     {
    //         char rom_code_s[OWB_ROM_CODE_STRING_LENGTH];
    //         owb_string_from_rom_code(rom_code, rom_code_s, sizeof(rom_code_s));
    //         printf("Single device %s present\n", rom_code_s);
    //     }
    //     else
    //     {
    //         printf("An error occurred reading ROM code: %d", status);
    //     }
    // }
    // else
    // {
    //     // Search for a known ROM code (LSB first):
    //     // For example: 0x1502162ca5b2ee28
    //     OneWireBus_ROMCode known_device = {
    //         .fields.family = { 0x28 },
    //         .fields.serial_number = { 0xee, 0xb2, 0xa5, 0x2c, 0x16, 0x02 },
    //         .fields.crc = { 0x15 },
    //     };
    //     char rom_code_s[OWB_ROM_CODE_STRING_LENGTH];
    //     owb_string_from_rom_code(known_device, rom_code_s, sizeof(rom_code_s));
    //     bool is_present = false;

    //     owb_status search_status = owb_verify_rom(owb, known_device, &is_present);
    //     if (search_status == OWB_STATUS_OK)
    //     {
    //         printf("Device %s is %s\n", rom_code_s, is_present ? "present" : "not present");
    //     }
    //     else
    //     {
    //         printf("An error occurred searching for known device: %d", search_status);
    //     }
    // }
}

float DS18b20Driver::readTemp()
{
    if( num_devices == 0)
    {
        printf("0 owb devices found\n");
        searchDevices();
        return 0;
    }
    printf("\nTemperature readings (degrees C):\n");
    ds18b20_convert_all(owb);
    //     In this application all devices use the same resolution,
    // so use the first device to determine the delay
    ds18b20_wait_for_conversion(devices[0]);
    float temp;
    for (int i = 0; i < num_devices; ++i)
    {
        ds18b20_read_temp(devices[i], &temp);
        //ds18b20_get_temp(devices[i]);
        printf("  %d: %.3f\n", i, temp);
    }
    return temp;
}

void DS18b20Driver::teardown()
{
    for (int i = 0; i < num_devices; ++i)
    {
        ds18b20_free(&devices[i]);
    }
    owb_uninitialize(owb);
}

DS18b20Driver::~DS18b20Driver()
{
    teardown();
}
