#include"../headers/NetworkControler.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

const std::string NetworkControler::TAG = "\nwifi softSTA";

NetworkControler::NetworkControler(std::string ssid, std::string password):
EXAMPLE_ESP_WIFI_SSID(ssid),
EXAMPLE_ESP_WIFI_PASS(password)
{}

static int EXAMPLE_ESP_MAXIMUM_RETRY = 10;
int s_retry_num = 0;
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1
const static char* TAG = "STA";
static EventGroupHandle_t s_wifi_event_group;

static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

void NetworkControler::initAndStartWifiAsSTA()
{

    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        event_handler,//&wifi_event_handler,
                                                        NULL,
                                                        NULL));

    wifi_config_t wifi_config = {};
    
 
    memcpy(wifi_config.sta.ssid,EXAMPLE_ESP_WIFI_SSID.c_str(),sizeof(wifi_config.ap.ssid));
    memcpy(wifi_config.sta.password,EXAMPLE_ESP_WIFI_PASS.c_str(),sizeof(wifi_config.ap.password));
    wifi_config.sta.threshold.authmode = WIFI_AUTH_WPA2_PSK;
    wifi_config.sta.pmf_cfg.capable = true;
    wifi_config.sta.pmf_cfg.capable = false;
    //wifi_config.sta.authmode = WIFI_AUTH_WPA_WPA2_PSK;
    // wifi_config.ap.ssid_len = (u_int8_t)EXAMPLE_ESP_WIFI_SSID.length();
    // wifi_config.ap.channel = EXAMPLE_ESP_WIFI_CHANNEL;
    // wifi_config.ap.max_connection = EXAMPLE_MAX_STA_CONN;
    // ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    printf("%s wifi_init_sta finished. SSID:%s password:%s \n", TAG.c_str(),
    EXAMPLE_ESP_WIFI_SSID.c_str(),EXAMPLE_ESP_WIFI_PASS.c_str());
    // ESP_LOGI(TAG.c_str(), "wifi_init_softap finished. SSID:%s password:%s channel:%d",
    //          EXAMPLE_ESP_WIFI_SSID.c_str(), EXAMPLE_ESP_WIFI_PASS.c_str(), EXAMPLE_ESP_WIFI_CHANNEL);
}
