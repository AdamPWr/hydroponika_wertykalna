#include"driver/gpio.h"
#include"esp_err.h"
#include<map>

#include"../headers/common.h"
#include"../headers/wifiManager.h"
#include"../headers/JSONParser.h"
#include"../headers/IOSensors.h"


namespace common
{


    void makePins12_14UsebleAsGpio()
    {
        gpio_config_t gpioConfig;
        gpioConfig.pin_bit_mask = (1 << GPIO_NUM_12) |  (1 << GPIO_NUM_13) | (1 << GPIO_NUM_14) | (1 << GPIO_NUM_17) | (1 << GPIO_NUM_17);
        gpioConfig.pull_down_en = GPIO_PULLDOWN_DISABLE;
        gpioConfig.mode = GPIO_MODE_INPUT_OUTPUT;
        ESP_ERROR_CHECK(gpio_config(&gpioConfig));
    }

    void makeSensorsMeasurmentsAndPublishData(std::shared_ptr<SystemComponents> systemComponents)
    {
        static std::map<std::string, float> sensorValues;
        static int value;

        if(systemComponents->stateManager.getConnectionState() == ConnectionState::STA_CONNECTED)
        {
            value = systemComponents->ds.readTemp();
            sensorValues.insert({"water_temperature", value});
            value =systemComponents->tdsDriver.calculateTDSVaule(value);
            sensorValues.insert({"tds", value});
            value = systemComponents->waterLevelDriver.checkWaterLevelMin();
            sensorValues.insert({"water_level_min", value});
            value = systemComponents->waterLevelDriver.checkWaterLevelMax();
            sensorValues.insert({"water_level_max", value});
            std::string data = JSONParser::parseToJson(sensorValues);
            printf(data.c_str());
            systemComponents->mqttclient.publish(MQTTClientHydroponika::data_topic, data);
            sensorValues.clear();
        }
        else{
            //TODO ma zapalac diode
            printf("Lost STA connection, Trying to reconnect.\n");
        }
    }

    void waterPumpTask(void * p_pump_controler)
    {
        PumpsControler* pump_controler = static_cast<PumpsControler*>(p_pump_controler);

        while(true)
        {
            pump_controler->turnOnPumpWithIntensity(GPIO_NUM_13,100);
            vTaskDelay(1000.0 * 60 *30/ portTICK_PERIOD_MS);
            pump_controler->turnOffPump(GPIO_NUM_13);
            vTaskDelay(1000.0 * 60 * 30/ portTICK_PERIOD_MS);
        }
    }
}