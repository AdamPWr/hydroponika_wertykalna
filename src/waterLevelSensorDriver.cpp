#include"../headers/waterLevelSensorDriver.h"
#include "driver/gpio.h"

WaterLevelSensorDriver::WaterLevelSensorDriver(gpio_num_t sensor_pin_min, gpio_num_t sensor_pin_max) :
WATER_LEVEL_MIN_SENSOR_PIN(sensor_pin_min),
WATER_LEVEL_MAX_SENSOR_PIN(sensor_pin_max)
{
    setup();
}

void WaterLevelSensorDriver::setup()
{
    //zero-initialize the config structure.
    gpio_config_t io_conf = {};
    //disable interrupt
    io_conf.intr_type = GPIO_INTR_DISABLE;
    //set as input mode
    io_conf.mode = GPIO_MODE_INPUT;
    //bit mask of the pins that you want to set,e.g.GPIO18/19
    io_conf.pin_bit_mask = (1ULL<<WATER_LEVEL_MIN_SENSOR_PIN) || (1ULL<<WATER_LEVEL_MAX_SENSOR_PIN);
    //disable pull-down mode
    io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE;
    //disable pull-up mode
    io_conf.pull_up_en = GPIO_PULLUP_DISABLE;
    //configure GPIO with the given settings
    gpio_config(&io_conf);
    
}

int WaterLevelSensorDriver::checkWaterLevelMin()
{
    int val = gpio_get_level(WATER_LEVEL_MIN_SENSOR_PIN);

    printf("Water min %d \n", val);
    return val;
}

int WaterLevelSensorDriver::checkWaterLevelMax()
{
    return gpio_get_level(WATER_LEVEL_MAX_SENSOR_PIN);
}