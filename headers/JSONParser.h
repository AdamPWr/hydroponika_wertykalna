#pragma once
#include<string>
#include<vector>
#include<map>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"


class JSONParser
{

    public:
    static std::string parseFromJson(std::string raw_content);
    static std::string parseToJson(std::map<std::string, float>& vaules);
    static std::string parseToJson(std::map<std::string, std::string>& vaules);
};